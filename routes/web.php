<?php

use App\Http\Controllers\Web\CardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('cards/create', [CardController::class, 'create'])->name('cards.create');
Route::post('cards/store', [CardController::class, 'store'])->name('cards.store');
Route::get('cards/show/{card}', [CardController::class, 'show'])->name('cards.show');
