<?php

declare(strict_types=1);

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Card;
use Illuminate\Http\Request;

class CardController extends Controller
{
    public function create()
    {
        $count = Card::query()->count();
        return view('cards.create', ['count' => $count]);
    }

    public function store(Request $request)
    {
        $card = new Card();
        $card->title = $request->title;
        $card->bonus = $request->bonus;
        $card->save();

        return redirect()->route('cards.show', ['card' => $card]);
    }

    public function show(Card $card)
    {
        return view('cards.show', ['card' => $card]);
    }
}
