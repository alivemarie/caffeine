<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## To run this project

- run `npm install`
- run `composer install`
- make `.env` file with variables from _.env.example_ and fill it with your actual DB variables, etc
- `php artisan migrate`
- `php artisan key:generate`
- run `php artisan serve`
