<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create card</title>
</head>
<body>
<div>Total cards count: {!! $count !!}</div>
<form action="{{route('cards.store')}}" method="post">
    @csrf
    <input type="text" name="title" placeholder="title">
    <br>
    <input type="number" name="bonus" placeholder="bonus">
    <br>
    <button type="submit">Save</button>
</form>
</body>
</html>

